/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

/* Global variables and functions */
let portail;

require('./bootstrap');
require('slick-carousel');
require('./includes/_section-animation');
require('./includes/_main');
require('./includes/_sliders');

