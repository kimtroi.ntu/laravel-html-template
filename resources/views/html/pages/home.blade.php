@extends('html.layout')

@section('metaTitle', 'Home')

@section('bodyClass', 'home')

@section('content')
    @include('html.partials.home.appointments')
@endsection

