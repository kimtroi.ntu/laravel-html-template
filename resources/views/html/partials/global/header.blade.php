<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('metaTitle') - HTML template</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,500,600,700,800,900" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('assets/css/app.css') }}">
</head>
<body class="@yield('bodyClass')">
<header id="header" class="header">
    <div class="block-header">
        <div class="container">
            <a href="#" class="logo" title="Portail Gate">
                <img src="{{ asset('assets/images/logo.png') }}" class="img" alt="Portail Gate Logo">
                <img src="{{ asset('assets/images/logo-white.png') }}" class="img-sticky" alt="Portail Gate Logo">
            </a>

            <a href="#" class="btn-search-mobile" title="Recherche"><i class="i-search"></i></a>

            <a href="#" class="btn-collapse" title="">
                <span class="left"></span>
                <span class="right"></span>
                <span class="sr-only">Menu</span>
            </a>

            <div class="wrap-collapse">
                <div class="feature" style="background-image: url('{{ asset('assets/images/menu.png') }}')"></div>

                <nav class="main-nav" role="navigation">
                    <div class="wrap">
                        <a href="#" class="logo-nav" title="Portail Gate"><img src="{{ asset('assets/images/logo-white.png') }}" alt="Portail Gate logo"></a>
                        <ul class="main-menu">
                            <li class="is-active"><a href="#">Accueil</a></li>
                            <li><a href="#">Découvrir le GATE</a></li>
                            <li><a href="#">Informations pratiques</a></li>
                            <li><a href="#">Préparer son arrivée</a></li>
                            <li><a href="#">Ils en parlent</a></li>
                            <li><a href="#">Partenaires et RDV</a></li>
                            <li><a href="#">Les évènements du GATE</a></li>
                        </ul>
                        <div class="contact">
                            <a href="#" class="help"><i class="i-question"></i><span>Aide</span></a>
                            <a href="mailto:" class="mail"><i class="i-email"></i><span>Contact</span></a>

                            <div class="multi-lang">
                                <ul>
                                    <li class="is-active fr"><a href="#">FR</a></li>
                                    <li class="en"><a href="#">EN</a></li>
                                    <li class="en"><a href="#">EN</a></li>
                                </ul>
                            </div>
                        </div>
                        <ul class="site-info">
                            <li><a href="#">Mention légales</a></li>
                            <li>.</li>
                            <li><a href="#">Chartes des données personnelles</a></li>
                            <li>.</li>
                            <li><a href="#">Plan du site</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="block-banner">
        <div class="container">
            <div class="content">
                <h1 class="title-1">Bienvenue au GATE</h1>
                <h2 class="title-2">Guichet Accueil des Talents Étrangers</h2>
                <div class="detail">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ex dolor, ornare et tellus id, vestibulum semper ligula. Ut vel est lorem. Duis purus erat, facilisis sit amet velit sed, efficitur venenatis felis. Vivamus risus tortor.
                </div>
                <div class="group-btn">
                    <a href="#" class="btn-arrow">Découvrir le GATE <i class="i-arrow-right"></i></a>
                    <a href="#" class="btn-arrow">Partenaires et RDV <i class="i-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <div class="feature" style="background-image: url('{{ asset('assets/images/banner.png') }}')">
            <div id="info" class="info">
                <a href="#" class="btn-close" title="Close" data-close="#info"><i class="i-close"></i></a>
                <span class="icon">
                    <i class="i-alert"></i>
                </span>
                <p class="title">Flash Info</p>
                <p class="des">Exceptionnellement le GATE sera fermée ce jour. Les RDV sont annulés et vous serez recontacté ultérieurement pour un nouveau RDV. Merci de votre compréhension.</p>
            </div>
        </div>
    </div>
    <div id="search" class="block-search">
        <form action="#" class="form-default form-search" method="get">
            <div class="input-wrap">
                <input id="input-s" type="text" placeholder="Recherche" class="input-field">
                <label class="input-label" for="input-s">
                    <button class="btn-submit" type="submit">Recherche</button>
                    <span class="input-label-content">Recherche</span>
                </label>
            </div>
        </form>
    </div>
</header>