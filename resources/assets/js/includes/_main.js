/* Global variables and functions */
portail = (function ($, window, undefined) {
    'use strict';
    let $win = $(window),
        $header = $('#header'),
        $main = $('#main'),
        $footer = $('#footer');

    /*-----------------------------------------------------*/
    /*------------------------  focus  --------------------*/
    /*-----------------------------------------------------*/

    function _tabFocus() {
        $('a[href], button').on('keyup focusout', function(e) {
            let code = (e.keyCode ? e.keyCode : e.which);
            if (code == 9) {
                $(this).addClass('is-focus');
                if($('.multi-lang li.is-active a').hasClass('is-focus')) {
                    $('.multi-lang li.is-active').addClass('is-clicked');
                    $('.multi-lang li:not(.is-active)').show();
                }
            } else  {
                $(this).removeClass('is-focus');

                if($(this).closest('li').is(':last-child') && $(this).closest('.multi-lang')) {
                    $('.multi-lang li.is-active').removeClass('is-clicked');
                    $('.multi-lang li:not(.is-active)').hide();
                }
            }
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  menu  ---------------------*/
    /*-----------------------------------------------------*/

    function _menu() {
        $('.btn-collapse').on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).toggleClass('is-active');
            $header.toggleClass('is-active');
            $('.wrap-collapse').toggleClass('is-active');
            $('body').toggleClass('disableScroll');
            // $('.block-search').slideUp();
        });

        /*-----------  detect tab keyboard  -----------*/

        $('.btn-collapse').on('keyup', function(e) {
            let code = (e.keyCode ? e.keyCode : e.which);
            if (code == 9) {
                e.preventDefault();
                e.stopPropagation();
                $(this).toggleClass('is-active');
                $header.toggleClass('is-active');
                $('.wrap-collapse').toggleClass('is-active');
                $('body').toggleClass('disableScroll');
            }
        });

        $('.main-nav a:last').on('focusout', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $('.btn-collapse').removeClass('is-active');
            $header.removeClass('is-active');
            $('.wrap-collapse').removeClass('is-active');
            $('body').removeClass('disableScroll');
        });
    }

    /*-----------------------------------------------------*/
    /*--------------------  sticky menu  ------------------*/
    /*-----------------------------------------------------*/

    function _stickyMenu() {
        let $nav = $('.block-header');
        let $banner = $('.block-banner');
        if($banner.length) {
            // let $mainContent = $('.main');
            let $navOffset = $banner.offset().top + $banner.outerHeight() - 60;

            $(window).scroll(function(){
                let scrollTop = $(this).scrollTop();
                if(scrollTop >= $navOffset) {
                    $nav.addClass('is-sticky');
                    $header.addClass('is-sticky');
                    // $mainContent.addClass('sticky');
                } else {
                    $nav.removeClass('is-sticky');
                    $header.removeClass('is-sticky');
                    // $mainContent.removeClass('sticky');
                }
            });
        }
    }

    /*-----------------------------------------------------*/
    /*---------------------  btn close   ------------------*/
    /*-----------------------------------------------------*/

    function _btnClose() {
        $('[data-close]').each(function () {
            let $target = $('[data-close]').data('close');

            $(this).click(function () {
                $($target).fadeOut();
            });
        });
    }

    /*-----------------------------------------------------*/
    /*--------------------  list even  --------------------*/
    /*-----------------------------------------------------*/

    function _listEvents() {
        let $btn = $('.list-events .btn-default');
        $btn.each(function () {
            $(this).not('.added-js').click(function (e) {
                e.preventDefault();
                $(this).closest('.item').toggleClass('is-active');
                $(this).closest('.item').find('.des').slideToggle();
            });
            $(this).addClass('added-js');
        });

    }

    /*-----------------------------------------------------*/
    /*------------------------  menu  ---------------------*/
    /*-----------------------------------------------------*/

    function _searchMobile() {
        $('.btn-search-mobile').on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('.block-search').slideToggle();
        });

        $('.btn-search-mobile').on('keyup', function(e) {
            let code = (e.keyCode ? e.keyCode : e.which);
            if (code == 9) {
                e.preventDefault();
                e.stopPropagation();
                $('.block-search').slideToggle();
            }
        });
    }

    /*-----------------------------------------------------*/
    /*-----------------------  select2  --------------------*/
    /*-----------------------------------------------------*/

    function _select2() {
        // let $select = $(".select2, .form-partner select");
        // if ($select.length) {
        //     $select.each(function () {
        //         $(this).select2({
        //             minimumResultsForSearch: -1,
        //         });
        //     });
        //
        // }
        // return $select;
    }

    /*-----------------------------------------------------*/
    /*---------------------  multi lang  ------------------*/
    /*-----------------------------------------------------*/

    function _multiLang() {
        $('.multi-lang li.is-active').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('is-clicked');
            $('.multi-lang li:not(.is-active)').toggle();
        });
    }

    /*-----------------------------------------------------*/
    /*-----------------------------------------------------*/
    /*-----------------------------------------------------*/

    function _ctAlert() {
        alert('welcome');
    }


    return {
        init: function () {
            _menu();
            _stickyMenu();
            _btnClose();
            _listEvents();
            _searchMobile();
            _select2();
            _multiLang();
        },
        ctAlert: _ctAlert,
        eventClick: _listEvents,
        tabFocus: _tabFocus,
    };

}(jQuery, window));

jQuery(document).ready(function () {
    portail.init();
});

jQuery(window).on('load', function () {
    portail.tabFocus();
});