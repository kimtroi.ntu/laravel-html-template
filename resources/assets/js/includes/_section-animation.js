/*
 * @file
 *
 * Available variables
 * - gulp_display
 *
 */

import * as ScrollMagic from "scrollmagic"; // Or use scrollmagic-with-ssr to avoid server rendering problems
import { TweenMax, TimelineMax } from "gsap"; // Also works with TweenLite and TimelineLite
import { ScrollMagicPluginGsap } from "scrollmagic-plugin-gsap";

ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax);

(function ($) {
    'use strict';

	function sectionAnim() {
		var controller = new ScrollMagic.Controller();
		var scene;

		/* ########################################################################################### */
		/* ---------------------------------- Animate || fadeInUp ------------------------------------ */
		/* ########################################################################################### */

		var $bBanner = ['.block-banner .content .title-1', '.block-banner .content .title-2 ', '.block-banner .content .detail', '.block-banner .content .group-btn', '.block-banner .feature'];
		var $bBannerInter = ['.block-banner-inter .content h1', '.block-banner-inter .des'];
		var $bSearch = ['.header:not(".is-sticky") .block-search'];
		var $bAppointments = ['.block-appointments .feature', '.block-appointments .is-slick-control', '.block-appointments .block-filter', '.block-appointments .list-appointments'];
		var $bInfomation = ['.block-infomations .content h2', '.block-infomations .btn-arrow', '.block-infomations .feature', '.block-infomations .map'];
		var $bEvents = ['.block-events .heading', '.block-events .no-event', '.block-events .content h2', '.block-events .content .des', '.block-events .content .btn-arrow', '.block-events .loadmore'];
		var $bFeature = ['.block-feature .thumb', '.block-feature .content h1', '.block-feature .content .des', '.block-feature .feature'];
		var $bStatistic = ['.block-statistic .content h2', '.block-statistic .content .des', '.block-statistic .btn-arrow'];

		if($('.block-banner').length) {
			_tweenStepbyStep($bBanner,'.block-banner', 1.5, 0.15);
		}

		if($('.block-banner-inter').length) {
			_tweenStepbyStep($bBannerInter,'.block-banner-inter', 1.5, 0.15);
		}

		if($('.header:not(".is-sticky") .block-search').length) {
			_tweenStepbyStep($bSearch,'.block-search', 1.5, 0.15);
		}

		if($('.block-appointments').length) {
			_tweenStepbyStep($bAppointments,'.block-appointments', 1.5, 0.15);
		}

		if($('.block-infomations').length) {
			_tweenStepbyStep($bInfomation,'.block-infomations', 1.5, 0.15);
			_tweenStepbyStep('.list-address .item','.block-infomations', 1.5, 0.25);
		}

		if($('.block-events').length) {
			_tweenStepbyStep($bEvents,'.block-events', 1.5, 0.15);
			_tweenStepbyStep('.list-events .item','.block-events', 1.5, 0.35);
		}

		if($('.block-feature').length) {
			_tweenStepbyStep($bFeature,'.block-feature', 1.5, 0.15);
		}

		if($('.block-statistic').length) {
			_tweenStepbyStep($bStatistic,'.block-statistic', 1.5, 0.15);
			_tweenStepbyStep('.list-statistic .item','.block-statistic', 1.5, 0.15);
		}

		$('.block-content-page').each(function() {
			_tweenStepbyStep('.block-content-page','.block-content-page', 1.5, 0.15);
		});

		function _tweenStepbyStep(blockList, blockParent, duration, delay) {
			var tweenLM = new TimelineMax();

			$(blockList).each(function (index) {
				var _this = this;
				if($(this).length) {
					tweenLM.from($(_this), duration, {
						autoAlpha: 0,
						y: 40,
						ease: Expo.easeOut,
						delay: delay*(index+1)
					}, 0.1);
				}
			});

			scene = new ScrollMagic.Scene({
				triggerElement: blockParent,
				reverse: false,
				triggerHook: '1',
			})
				.setTween(tweenLM)
				.addTo(controller)
			;
		}

		/* ########################################################################################### */
		/* ------------------------------------------------------------------------------------------- */
		/* ########################################################################################### */
	}

	$(document).ready(function () {
		sectionAnim();
	});


})(jQuery);
