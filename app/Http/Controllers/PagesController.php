<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        return view('html.pages.home');
    }

    public function contact()
    {
        return view('pages.contact');
    }
}
