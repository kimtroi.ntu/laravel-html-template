@include('html.partials.global.header')

<main id="main-content" class="main">
    @yield('content')
</main>

@include('html.partials.global.footer')